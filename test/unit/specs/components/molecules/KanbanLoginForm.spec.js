import { mount } from '@vue/test-utils'
import KanbanLoginForm from '@/components/molecules/KanbanLoginForm.vue'

describe('KanbanLoginForm', () => {
  describe('property', () => {
    describe('validation', () => {
      let loginForm
      beforeEach(done => {
        loginForm = mount(KanbanLoginForm, {
          propsData: { onlogin: () => {} }
        })
        loginForm.vm.$nextTick(done)
      })

      describe('email', () => {
        describe('required', () => {
          describe('何も入力されていない', () => {
            it('validation.email.requiredがinvalidであるべき', () => {
              loginForm.setData({ email: '' })
              expect(loginForm.vm.validation.email.required).to.equal(false)
            })
          })

          describe('入力あり', () => {
            it('validation.email.requiredがvalidであるべき', () => {
              loginForm.setData({ email: 'example@domain.com' })
              expect(loginForm.vm.validation.email.required).to.equal(true)
            })
          })
        })
      })

      describe('format', () => {
        describe('メールアドレス形式でないフォーマット', () => {
          it('validation.email.formatがinvalidであるべき', () => {
            loginForm.setData({ email: 'hogepiyo' })
            expect(loginForm.vm.validation.email.format).to.equal(false)
          })
        })

        describe('メールアドレス形式のフォーマット', () => {
          it('validation.email.formatがvalidであるべき', () => {
            loginForm.setData({ email: 'example@domain.com' })
            expect(loginForm.vm.validation.email.format).to.equal(true)
          })
        })
      })

      describe('password', () => {
        describe('何も入力されていない', () => {
          it('validation.password.requiredがinvalidであるべき', () => {
            loginForm.setData({ password: 'hogepiyo' })
            expect(loginForm.vm.validation.password.required).to.equal(false)
          })
        })

        describe('入力あり', () => {
          it('validation.password.requiredがvalidであるべき', () => {
            loginForm.setData({ password: 'example@domain.com' })
            expect(loginForm.vm.validation.password.required).to.equal(true)
          })
        })
      })
    })
  })

  describe('valid', () => {
    let loginForm
    beforeEach(done => {
      loginForm = mount(KanbanLoginForm, {
        propsData: { onlogin: () => {} }
      })
      loginForm.vm.$nextTick(done)
    })

    describe('バリデーション項目すべてOK', () => {
      it('validになるべき', () => {
        loginForm.setData({
          email: 'example@domain.com',
          password: '1234567890'
        })
        expect(loginForm.vm.valid).to.equal(true)
      })
    })

    describe('バリデーションNG項目あり', () => {
      it('invalidになるべき', () => {
        loginForm.setData({
          email: 'example@domain.com',
          password: ''
        })
        expect(loginForm.vm.valid).to.equal(false)
      })
    })
  })

  describe('disableLoginAction', () => {
    let loginForm
    beforeEach(done => {
      loginForm = mount(KanbanLoginForm, {
        propsData: { onlogin: () => {} }
      })
      loginForm.vm.$nextTick(done)
    })

    describe('バリデーションNG項目あり', () => {
      it('ログイン処理無効', () => {
        loginForm.setData({
          email: 'example@domain.com',
          password: ''
        })
        expect(loginForm.vm.disableLoginAction).to.equal(false)
      })
    })

    describe('バリデーションすべてOKかつログイン処理中ではない', () => {
      it('バリデーションは有効だがログイン処理無効', () => {
        loginForm.setData({
          email: 'example@domain.com',
          password: '1234567890'
        })
        expect(loginForm.vm.disableLoginAction).to.equal(false)
      })
    })

    describe('バリデーションすべてOKかつログイン処理中である', () => {
      it('バリエーション、ログイン処理有効', () => {
        loginForm.setData({
          email: 'example@domain.com',
          password: '1234567890',
          pprogress: true
        })
        expect(loginForm.vm.disableLoginAction).to.equal(true)
      })
    })
  })

  describe('onlogin', () => {
    let loginForm
    let onloginStub
    beforeEach(done => {
      onloginStub = sinon.stub()
      loginForm = mount(KanbanLoginForm, {
        propsData: { onlogin: onloginStub }
      })
      loginForm.setData({
        email: 'example@domain.com',
        password: '1234567890'
      })
      loginForm.vm.$nextTick(done)
    })

    describe('resolve', () => {
      it('resolveされるべき', done => {
        onloginStub.resolves()

        // クリックイベント
        loginForm.find('button').trigger('click')
        expect(onloginStub.called).to.equal(false) // まだresolveされない
        expect(loginForm.vm.error).to.equal('') // エラーメッセージ初期化
        expect(loginForm.vm.disableLoginAction).to.equal(true) // ログインアクション不可

        // 状態の反映
        loginForm.vm.$nextTick(() => {
          expect(onloginStub.called).to.equal(true) // resolveされた
          const authInfo = onloginStub.args[0][0]
          expect(authInfo.email).to.equal(loginForm.vm.email)
          expect(authInfo.password).to.equal(loginForm.vm.password)
          loginForm.vm.$nextTick(() => {
            // resolve内での状態反映
            expect(loginForm.vm.error).to.equal('') // エラーメッセージ初期化のまま
            expect(loginForm.vm.disableLoginAction).to.equal(false) // ログインアクション可能
            done()
          })
        })
      })
    })

    describe('reject', () => {
      it('rejectされるべき', done => {
        onloginStub.rejects(new Error('login error!'))

        // クリックイベント
        loginForm.find('button').trigger('click')
        expect(onloginStub.called).to.equal(false) // まだrejectされない
        expect(loginForm.vm.error).to.equal('') // エラーメッセージ初期化
        expect(loginForm.vm.disableLoginAction).to.equal(true) // ログインアクション不可

        // 状態の反映
        loginForm.vm.$nextTick(() => {
          expect(onloginStub.called).to.equal(true) // rejectされた
          const authInfo = onloginStub.args[0][0]
          expect(authInfo.email).to.equal(loginForm.vm.email)
          expect(authInfo.password).to.equal(loginForm.vm.password)
          loginForm.vm.$nextTick(() => {
            // reject内での状態反映
            expect(loginForm.vm.error).to.equal('login error!') // エラーメッセージが設定される
            expect(loginForm.vm.disableLoginAction).to.equal(false) // ログインアクション可能
            done()
          })
        })
      })
    })
  })
})
