import { mount } from '@vue/test-utils'
import KanbanButton from '@/components/atoms/KanbanButton.vue'

describe('KanbanButton', () => {
  describe('property', () => {
    describe('type', () => {
      describe('default', () => {
        it('button.kanban-buttonであるべき(type default)', () => {
          const button = mount(KanbanButton)
          expect(button.is('button')).to.equal(true)
          expect(button.classes()).to.include('kanban-button')
        })
      })

      describe('button', () => {
        it('button.kanban-buttonであるべき(type button)', () => {
          const button = mount(KanbanButton, {
            propsData: { type: 'button' }
          })
          expect(button.is('button')).to.equal(true)
          expect(button.classes()).to.include('kanban-button')
        })
      })

      describe('text', () => {
        it('button.kanban-buttonであるべき(type text)', () => {
          const button = mount(KanbanButton, {
            propsData: { type: 'button' }
          })
          expect(button.is('button')).to.equal(true)
          expect(button.classes()).to.include('kanban-button')
        })
      })

      describe('disabled', () => {
        describe('default', () => {
          it('disabled属性が付与されていないべき(default)', () => {
            const button = mount(KanbanButton)
            expect(button.attributes().disabled).to.be.an('undefined')
          })
        })

        describe('true', () => {
          it('disabled属性が付与されているべき', () => {
            const button = mount(KanbanButton, {
              propsData: { disabled: true }
            })
            expect(button.attributes().disabled).to.equal('disabled')
          })
        })

        describe('false', () => {
          it('disabled属性が付与されていないべき', () => {
            const button = mount(KanbanButton, {
              propsData: { disabled: false }
            })
            expect(button.attributes().disabled).to.be.an('undefined')
          })
        })
      })
    })

    describe('event', () => {
      describe('click', () => {
        it('clickイベントが発行されているべき', () => {
          const button = mount(KanbanButton)
          button.trigger('click')
          expect(button.emitted().click.length).to.equal(1)
        })
      })
    })

    describe('slot', () => {
      describe('contents inserted', () => {
        it('コンテンツが挿入されているべき', () => {
          const button = mount(KanbanButton, {
            slots: { default: '<p>hello</p>' }
          })
          expect(button.text()).to.equal('hello')
        })
      })

      describe('no contents insert', () => {
        it('コンテンツが挿入されていないべき', () => {
          const button = mount(KanbanButton)
          expect(button.text()).to.equal('')
        })
      })
    })
  })
})
