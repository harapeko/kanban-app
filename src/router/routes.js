import KanbanBoardView from '@/components/templates/KanbanBoardView.vue'
import KanbanLoginView from '@/components/templates/KanbanLoginView.vue'
import KanbanTaskDetailModal from '@/components/templates/KanbanTaskDetailModal.vue'

export default[{
  path: '/',
  component: KanbanBoardView,
  meta: { reuiresAuth: true }
}, {
  path: '/login',
  component: KanbanLoginView
}, {
  path: '/',
  component: KanbanTaskDetailModal,
  meta: { reuiresAuth: true }
}]
